/**
 *  A module representing ModelGroup.
 *  @namespace Editor3d/Scene
 */
define(
    'Editor3d/Scene/ModelGroup/model-group-module',
    ['THREE'],
    function ModelGroupModule (THREE) {
        "use strict";

        /////////////////// API
        /**
         * @class
         * @memberof ModelGroup
         */
        var ModelGroup = {
            addModelGroupToScene: addModelGroupToScene
        };

        /////////////////// Methods
        /**
         * Adds model group to scene.
         *
         * @memberof ModelBody
         * @param {Object} scene - Scene.
         */
        function addModelGroupToScene (scene) {
            var group = new THREE.Group();

            scene.add(group);

            return group;
        }

        return ModelGroup;
    }
);