/**
 *  A module representing Lights.
 *  @namespace Editor3d/Scene
 */
define(
    'Editor3d/Scene/Lights/lights-module',
    ['THREE'],
    function LightsModule(THREE) {
        "use strict";

        /////////////////// API
        /**
         * @class
         * @memberof Lights
         */
        var Lights = {
            addDefaultLightsToScene: addDefaultLightsToScene
        };

        /////////////////// Private Methods
        /**
         * Adds directional lights to scene.
         *
         * @private
         * @memberof Lights
         * @param {Object} scene - Scene.
         */
        function _addDirectionalLightsToScene(scene) {
            var directionalLightArr = _createDirectionalLights();

            for (var i = 0, len = directionalLightArr.length; i < len; i++) {
                scene.add(directionalLightArr[i]);
            }
        }

        /**
         * Adds hemisphere light to scene.
         *
         * @private
         * @memberof Lights
         * @param {Object} scene - Scene.
         */
        function _addHemisphereLightToScene(scene) {
            var hemiLight = new THREE.HemisphereLight(0xffffff, 0xffffff, 0.6);

            hemiLight.color.setHSL(0.6, 1, 1);
            hemiLight.groundColor.setHSL(0.095, 1, 0.75);
            hemiLight.position.set(0, 0, 1000);

            scene.add(hemiLight);
        }

        /**
         * Creates directional lights.
         *
         * @private
         * @memberof Lights
         * @return {Array}
         */
        function _createDirectionalLights() {
            var directionalLightArr = [],
                d = 50;

            for (var i = 0; i < 2; i++) {
                directionalLightArr[i] = new THREE.DirectionalLight(0xffffff, 1);
                directionalLightArr[i].color.setHSL(0.1, 1, 0.95);
                directionalLightArr[i].position.multiplyScalar(50);

                directionalLightArr[i].castShadow = true;

                directionalLightArr[i].shadowMapWidth = 2048;
                directionalLightArr[i].shadowMapHeight = 2048;

                directionalLightArr[i].shadowCameraLeft = -d;
                directionalLightArr[i].shadowCameraRight = d;
                directionalLightArr[i].shadowCameraTop = d;
                directionalLightArr[i].shadowCameraBottom = -d;

                directionalLightArr[i].shadowCameraFar = 3500;
                directionalLightArr[i].shadowBias = -0.0001;
                directionalLightArr[i].shadowDarkness = 0.35;
            }

            directionalLightArr[0].position.set(0, 0, 60);
            directionalLightArr[1].position.set(0, 0, -60);

            return directionalLightArr;
        }

        /////////////////// Methods
        /**
         * Adds default lights to scene.
         *
         * @memberof Lights
         * @param {Object} scene - Scene.
         */
        function addDefaultLightsToScene(scene) {
            _addDirectionalLightsToScene(scene);
            _addHemisphereLightToScene(scene);
        }

        return Lights;
    }
);