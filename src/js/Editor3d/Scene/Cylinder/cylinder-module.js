/**
 *  A module representing Cylinder.
 *  @namespace Editor3d/Scene
 */
define(
    'Editor3d/Scene/Cylinder/cylinder-module',
    [
        'THREE',
        'Editor3d/Scene/Axis/axis-module',
        'Configuration/configuration-module',
        'Utils/utils-module',

        'Model/Body',
        'CodeEditor/Parser/parser-module'
    ],
    function CylinderModule(THREE, Axis, config, Utils, Body, Parser) {
        "use strict";

        /////////////////// API
        /**
         * @class
         * @memberof Cylinder
         */
        var Cylinder = {
            addCylinderToScene: addCylinderToScene,
            addCylinderAsPartToScene: addCylinderAsPartToScene,
            editCylinderOnScene: editCylinderOnScene
        };

        /////////////////// Private Methods
        function _accumulateRotation(object, acc) {
            var partAsP2inJoints;

            if (object.model.dx.value !== 0 || object.model.dy.value !== 0 || object.model.dz.value !== 0) {
                acc.x += object.model.rx.value;
                acc.y += object.model.ry.value;
                acc.z += object.model.rz.value;

                partAsP2inJoints = Body.getJointsByPartRefNumberWithDistinction(object.model.p1.value).partAsP2;

                if (typeof partAsP2inJoints[0] !== 'undefined') {
                    acc = _accumulateRotation(partAsP2inJoints[0], acc);
                }
            }
            return acc;
        }

        /////////////////// Public Methods
        function addCylinderAsPartToScene(model, settings) {
            var object = Body.getObjectByLine(settings.line),
                cylinder, material, mesh;

            cylinder = new THREE.CylinderGeometry(1, 1, 1, 8, 1, false);

            material = new THREE.MeshLambertMaterial({wireframe: config.values.wireframe});
            material.color.setRGB(object.model.vr.value, object.model.vg.value, object.model.vb.value);

            mesh = new THREE.Mesh(cylinder, material);
            mesh.overdraw = true;

            mesh.scale.set(object.model.sx.value, object.model.sy.value, object.model.sz.value);

            if (!isNaN(object.model.rx.value) && !isNaN(object.model.ry.value) && !isNaN(object.model.rz.value)) {
                mesh.rotation.order = 'ZYX'; //follow the way Framsticks rotates Parts
                mesh.rotation.x = object.model.rx.value;
                mesh.rotation.y = -object.model.ry.value;
                mesh.rotation.z = object.model.rz.value;
            }

            mesh.position.x = object.model.x.value;
            mesh.position.y = object.model.y.value;
            mesh.position.z = object.model.z.value;

            // Fred specific properites
            mesh.f0type = 'p';
            mesh.line = settings.line;
            mesh.isDraggable = true;

            object.add3dRepresentation(mesh);
            model.add(mesh);

            if (typeof object.localAxes === 'undefined') {
                object.localAxes = Axis.addLocalAxesToObject(object.model3d);
                object.localAxes.visible = config.values.localAxes;
            }
        }

        /**
         * Adds Cylinder to scene with given settings.
         *
         * @memberof Cylinder
         * @param {Object} model - Group with 3d model on scene.
         * @param {Object} settings - Settings for new Cylinder.
         */
        function addCylinderToScene(model, settings) {
            var object = Body.getObjectByLine(settings.line),
                cylinder, material, mesh;

            var distance, position,
                orientation, offsetRotation,
                accumulatedRotation = {
                    x: 0,
                    y: 0,
                    z: 0
                };

            var vstart, vend;

            var p1 = Body.getObjectByRefNumber(object.model.p1.value, 'p'),
                p2 = Body.getObjectByRefNumber(object.model.p2.value, 'p');

            if (p1 !== null && p2 !== null &&
                p1.type === 'p' && p2.type === 'p' &&
                p1.model3d !== null && p2.model3d !== null) {

                if (object.model.dx.value !== 0 || object.model.dy.value !== 0 || object.model.dz.value !== 0) {

                    // Relative translation of point p2 according to p1 position and joint dx,dy,dz values
                    p2.model3d.position.x = p1.model3d.position.x + object.model.dx.value;
                    p2.model3d.position.y = p1.model3d.position.y + object.model.dy.value;
                    p2.model3d.position.z = p1.model3d.position.z + object.model.dz.value;

                    // Making position of p2 local before rotation
                    p2.model3d.position.x -= p1.model3d.position.x;
                    p2.model3d.position.y -= p1.model3d.position.y;
                    p2.model3d.position.z -= p1.model3d.position.z;

                    // rotation around world axes
                    accumulatedRotation = _accumulateRotation(object, accumulatedRotation);

                    Utils.rotateAboutWorldAxes(p2.model3d, {
                        x: accumulatedRotation.x,
                        y: accumulatedRotation.y,
                        z: accumulatedRotation.z
                    });

                    if (typeof p2.localAxes === 'undefined') {
                        p2.localAxes = Axis.addLocalAxesToObject(p2.model3d);
                        p2.localAxes.rotation.set(accumulatedRotation.x, -accumulatedRotation.y, accumulatedRotation.z, 'ZYX');
                    } else {
                        p2.localAxes.rotation.set(accumulatedRotation.x, -accumulatedRotation.y, accumulatedRotation.z, 'ZYX');
                    }

                    // Making position of p2 relative to p1 again, after rotation
                    p2.model3d.position.x += p1.model3d.position.x;
                    p2.model3d.position.y += p1.model3d.position.y;
                    p2.model3d.position.z += p1.model3d.position.z;
                } else if (typeof p2.localAxes !== 'undefined') {
                    p2.localAxes.rotation.set(0,0,0);
                }
                p2.localAxes.visible = config.values.localAxes;

                p1 = p1.model3d;
                p2 = p2.model3d;

                vstart = new THREE.Vector3(p1.position.x, p1.position.y, p1.position.z);
                vend = new THREE.Vector3(p2.position.x, p2.position.y, p2.position.z);

                distance = vstart.distanceTo(vend);
                position = vend.clone().add(vstart).divideScalar(2);

                material = new THREE.MeshLambertMaterial({wireframe: config.values.wireframe});

                if (object.model.sh.value === 0 && distance > object.model.dx.max) {
                    Parser.annotations.push({
                        row: object.line,
                        column: 1,
                        text: 'delta (' + distance.toFixed(2) + ') too big in joint #' + object.refNumber + '. Max: '+object.model.dx.max+'.',
                        type: 'error'
                    });
                    setTimeout(function () {
                        editor.getSession().setAnnotations(Parser.annotations);
                    }, 300);
                }

                if (object.model.sh.value === 1) {
                    cylinder = new THREE.CylinderGeometry(config.values.joint.invisible.radius, config.values.joint.invisible.radius, distance, 8, 1, false);

                    material.transparent = true;
                    material.opacity = config.values.joint.invisible.opacity;
                } else {
                    cylinder = new THREE.CylinderGeometry(config.values.joint.radius, config.values.joint.radius, distance, 8, 1, false);
                }

                material.color.setRGB(object.model.vr.value, object.model.vg.value, object.model.vb.value);

                orientation = new THREE.Matrix4();//a new orientation matrix to offset pivot
                offsetRotation = new THREE.Matrix4();//a matrix to fix pivot rotation

                orientation.lookAt(vstart, vend, new THREE.Vector3(0, 1, 0));//look at destination
                offsetRotation.makeRotationX(config.constants.HALF_PI);//rotate 90 degs on X
                orientation.multiply(offsetRotation);//combine orientation with rotation transformations
                cylinder.applyMatrix(orientation);

                mesh = new THREE.Mesh(cylinder, material);

                mesh.position.x = position.x;
                mesh.position.y = position.y;
                mesh.position.z = position.z;

                // Fred specific properites
                mesh.overdraw = true;
                mesh.f0type = 'j';
                mesh.p1 = object.model.p1.value;
                mesh.p2 = object.model.p2.value;
                mesh.line = settings.line;
                mesh.isDraggable = false;

                object.add3dRepresentation(mesh);
                model.add(mesh);
            }
        }

        /**
         * Edits Cylinder with given settings.
         *
         * @memberof Cylinder
         * @param {Object} model - Group with 3d model on scene.
         * @param {Object} settings - Settings for edited Cylinder.
         */
        function editCylinderOnScene(model, settings) {
            var cylinder = Body.get3dObjectByLine(settings.line);

            if (cylinder !== null) {
                Body.removeObjectFromSceneByLine(cylinder.line, model);
            }

            if (settings.type === 'j') {
                Cylinder.addCylinderToScene(model, settings);
            } else if (settings.type === 'p') {
                Cylinder.addCylinderAsPartToScene(model, settings);
            }
        }

        return Cylinder;
    }
);