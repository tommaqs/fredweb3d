/**
 *  A module representing Controls.
 *  @namespace Editor3d/Scene
 */
define(
    'Editor3d/Scene/Controls/controls-module',
    ['THREE', 'Configuration/configuration-module',],
    function ControlsModule (THREE, config) {
        "use strict";

        /////////////////// API
        /**
         * @class
         * @memberof Controls
         */
        var Controls = {
            // getters and setters
            get controls () { return controls; },

            // regular methods
            addGridHelperToScene: addGridHelperToScene,
            addPlaneMoveHelperToScene: addPlaneMoveHelperToScene,
            createDefaultControls: createDefaultControls
        };

        /////////////////// Data store
        var controls = null;

        /////////////////// Methods
        /**
         * Adds grid helper to scene.
         * It helps to understand where objects are located.
         *
         * @memberof Controls
         * @param {Object} scene - Scene.
         */
        function addGridHelperToScene (scene) {
            var grid = new THREE.GridHelper(100, 5);

            grid.setColors('#333', '#111');
            grid.rotation.x = 90 * Math.PI / 180;

            grid.visible = config.values.grid;

            scene.add(grid);

            return grid;
        }

        /**
         * Adds plane move helper to scene.
         * It helps with dragging objects in 3d world.
         *
         * @memberof Controls
         * @param {Object} scene - Scene.
         */
        function addPlaneMoveHelperToScene (scene) {
            var planeMoveHelper = new THREE.Mesh(
                new THREE.PlaneBufferGeometry( 2000, 2000, 8, 8 ),
                new THREE.MeshBasicMaterial( { color: 0xff0000, opacity: 0.7, transparent: true } )
            );

            planeMoveHelper.visible = false;

            scene.add( planeMoveHelper );

            return planeMoveHelper;
        }

        /**
         * Creates default controls.
         *
         * @memberof Controls
         * @param {Object} camera - Camera.
         * @param {Object} element - DOM element.
         * @returns {Object}
         */
        function createDefaultControls (camera, element) {
            var $embedSettingsEl = $('#embed-settings'),
                targetPos = {};

            controls = new THREE.OrbitControls(camera, element);

            controls.autoRotateSpeed = 4.0;
            controls.rotateSpeed = 2.0;
            controls.zoomSpeed = 1.2;
            controls.panSpeed = 0.4;
            controls.noZoom = false;
            controls.noPan = false;
            controls.staticMoving = true;
            controls.dynamicDampingFactor = 0.3;

            targetPos.x = (typeof window.FREDJS_EMBED_TARGET_X !== 'undefined' && window.FREDJS_EMBED_TARGET_X !== null) ? parseFloat(window.FREDJS_EMBED_TARGET_X) : $embedSettingsEl.data('targetX') || 0;
            targetPos.y = (typeof window.FREDJS_EMBED_TARGET_Y !== 'undefined' && window.FREDJS_EMBED_TARGET_Y !== null) ? parseFloat(window.FREDJS_EMBED_TARGET_Y) : $embedSettingsEl.data('targetY') || 0;
            targetPos.z = (typeof window.FREDJS_EMBED_TARGET_Z !== 'undefined' && window.FREDJS_EMBED_TARGET_Z !== null) ? parseFloat(window.FREDJS_EMBED_TARGET_Z) : $embedSettingsEl.data('targetZ') || 0;

            controls.target = new THREE.Vector3(targetPos.x, targetPos.y, targetPos.z);
            controls.autoRotate = config.values.autorotate;

            return controls;
        }

        return Controls;
    }
);