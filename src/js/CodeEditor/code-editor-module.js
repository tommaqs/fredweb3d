/**
 *  A module representing CodeEditor.
 *  @namespace CodeEditor
 */
define(
    'CodeEditor/code-editor-module',
    ['jquery', 'ace/ace', 'Model/Body', 'Configuration/configuration-module', 'Editor3d/editor3d-module', 'CodeEditor/Parser/parser-module'],
    function CodeEditorModule ($, ace, Body, config, Editor3d, Parser) {
        "use strict";

        /////////////////// API
        /**
         * @class
         * @memberof CodeEditor
         */
        var CodeEditor = {
            // getters
            get code        () { return data.code;      },
            get codeLines   () { return data.codeLines; },
            get editor      () { return data.editor;    },
            // setters
            set code        (code)      { data.code = code              },
            set codeLines   (codeLines) { data.codeLines = codeLines    },
            set editor      (editor)    { data.editor = editor          },
            
            init: init
        };

        /////////////////// Data store
        var data = {
                code: '',
                codeLines: null,
                editor: null
            },
            defaultCode = '//0' + config.constants.LINE_SEPARATOR;

        /////////////////// Private Methods
        /**
         * Trigger on changes in Body.
         *
         * It basically sets value to code editor when user change something in the model (e.g. by Editor3D).
         *
         * @private
         */
        function _onBodyChange () {
            if (!config.mode.embed) {
                CodeEditor.editor.setValue(Body.generateCodeFromModel(), 1);
            }
        }

        /**
         * Reacts on window resize event.
         *
         * @private
         */
        function _onWindowResize () {
            $('#editor').height(window.innerHeight - $('#top').outerHeight(true) - $('#editor-title').outerHeight(true) - $('#editor-toolbar').outerHeight(true));
            CodeEditor.editor.resize();
        }

        /**
         * Initialize Embed version of FredJS
         * @private
         */
        function _initEmbedFredJS () {
            var code;
            var commentsWithGenotype;
            var comment;

            if (window.FREDJS_EMBED_GENOTYPE !== null) {
                code = window.FREDJS_EMBED_GENOTYPE;
                code = code.replace(/\\n/g, '\n');
                _setCode(code);
            } else if (window.FREDJS_EMBED_DB_URL !== null) {
                $.ajax({
                    type: 'GET',
                    url: window.FREDJS_EMBED_DB_URL,
                    dataType: 'html'
                })
                    .success(function (data) {
                        var regex = /<!--[\s\S]*?-->/g;
                        var arr = data.match(regex);
                        var code;

                        for (var i = 0; i < arr.length; i++) {
                            if (arr[i].substring(0,12) === '<!--FRED_GEN') {
                                code = arr[i].substring(12,arr[i].length-3);
                            }
                        }

                        _setCode(code);
                    })
                    .fail(function( jqXHR, textStatus ) {
                        document.getElementById('error-container').style.display = 'block';
                        document.getElementById('error').innerHTML = "Request from param db-url failed: " + textStatus;
                    });
            } else {
                // get code from comment
                commentsWithGenotype = $('body').contents().filter(function(){
                    return this.nodeType === 8 && this.nodeValue.substring(0, 8).toUpperCase() === 'FRED_GEN';
                });
                comment = commentsWithGenotype[0];

                code = comment.nodeValue.substring(8);

                _setCode(code);
            }

            function _setCode (code) {
                CodeEditor.code = code;

                // split code into code lines
                CodeEditor.codeLines = CodeEditor.code.split(config.constants.LINE_SEPARATOR);

                // parse newly read code
                Parser.parseCode(CodeEditor.codeLines);

                // call the trigger for changes in Body
                Body.triggerEvent('change', _onBodyChange);
            }
        }

        /**
         * Initialize Full version of FredJS
         * @private
         */
        function _initFullFredJS () {
            // initialize and configure ace editor
            $('#editor').height(window.innerHeight - $('#top').outerHeight(true) - $('#editor-title').outerHeight(true) - $('#editor-toolbar').outerHeight(true));

            CodeEditor.editor = ace.edit("editor");
            CodeEditor.editor.setTheme("ace/theme/clouds_midnight");
            CodeEditor.editor.getSession().setMode("ace/mode/f0");
            CodeEditor.editor.defaultCode = defaultCode;

            //CodeEditor.editor.defaultCode += 'p:0,0,0,sh=3\n';
            //CodeEditor.editor.defaultCode += 'p:2,2,2,sh=2\n';

            /*CodeEditor.editor.defaultCode += 'p: 1.66, -1.4, 2.61\n';
            CodeEditor.editor.defaultCode += 'p: -0.4, -2.81, 2.5\n';
            CodeEditor.editor.defaultCode += 'p: 0.84, -2.73, 3.38\n';
            CodeEditor.editor.defaultCode += 'j: 0, 2\n';
            CodeEditor.editor.defaultCode += 'j: 2, 1\n';*/

            /*CodeEditor.editor.defaultCode += 'p:\n';
            CodeEditor.editor.defaultCode += 'p:\n';
            CodeEditor.editor.defaultCode += 'p:\n';
            CodeEditor.editor.defaultCode += 'p:\n';
            CodeEditor.editor.defaultCode += 'j:0,1,dx=1\n';
            CodeEditor.editor.defaultCode += 'j:1,2,dx=1,rz=0.5235\n';
            CodeEditor.editor.defaultCode += 'j:2,3,dx=1,rz=0.5235\n';*/

            /*CodeEditor.editor.defaultCode += 'p:\n';
            CodeEditor.editor.defaultCode += 'p:\n';
            CodeEditor.editor.defaultCode += 'p:\n';
            CodeEditor.editor.defaultCode += 'p:\n';
            CodeEditor.editor.defaultCode += 'p:\n';
            CodeEditor.editor.defaultCode += 'p:\n';
            CodeEditor.editor.defaultCode += 'j:0,1,dx=1,rz=1\n';
            CodeEditor.editor.defaultCode += 'j:1,2,dx=1,rz=1\n';
            CodeEditor.editor.defaultCode += 'j:2,3,dx=1,rz=1\n';
            CodeEditor.editor.defaultCode += 'j:3,4,dx=1,rz=1\n';
            CodeEditor.editor.defaultCode += 'j:4,5,dx=1,rz=1\n';*/


            // Struktura cykliczna. Obiekty typu Part połączone są w sekwencję 0->1->2->3->0
            /*CodeEditor.editor.defaultCode += 'p:0,0\n';
            CodeEditor.editor.defaultCode += 'p:1,0\n';
            CodeEditor.editor.defaultCode += 'p:1,1\n';
            CodeEditor.editor.defaultCode += 'p:0,1\n';
            CodeEditor.editor.defaultCode += 'j:0,1\n';
            CodeEditor.editor.defaultCode += 'j:1,2\n';
            CodeEditor.editor.defaultCode += 'j:2,3\n';
            CodeEditor.editor.defaultCode += 'j:3,0\n';*/

            // add events
            Body.on('change', _onBodyChange);
            window.addEventListener('resize', _onWindowResize, false);

            // synchronization method
            CodeEditor.editor.synchronizeWithView = function () {
                var lines = CodeEditor.editor.session.doc.$lines;

                Parser.parseCode(lines, CodeEditor.editor);

                Body.triggerEvent('change', _onBodyChange);
            };
            CodeEditor.editor.on('change', CodeEditor.editor.synchronizeWithView);

            // load default code into editor
            CodeEditor.editor.setValue(CodeEditor.editor.defaultCode, 1);

            // set focus on editor
            CodeEditor.editor.focus();

            window.editor = CodeEditor.editor;
        }
        
        /////////////////// Public Methods
        /**
         * Method prepares editor in proper mode
         *
         * @memberof CodeEditor
         */
        function init () {
            if (!config.mode.embed) {
                _initFullFredJS();
            } else {
                _initEmbedFredJS();
            }
        }

        /**
         * MOCKUPS DATA TO REMOVE AFTER TESTS:
         */

        // Pojedyncza pałeczka
        /*data.defaultCode += 'p:\n';
        data.defaultCode += 'p:1\n';
        data.defaultCode += 'j:0,1\n';*/

        // Trzy pałeczki w linii, brak opcji “delta” – użyto koordynaty absolutne
        /*data.defaultCode += 'p:\n';
        data.defaultCode += 'p:1,m=2\n';
        data.defaultCode += 'p:2,m=2\n';
        data.defaultCode += 'p:3,\n';
        data.defaultCode += 'j:0,1\n';
        data.defaultCode += 'j:1,2\n';
        data.defaultCode += 'j:2,3\n';*/

        // Trzy pałeczki w linii, użyto opcję “delta” – pozycjonowanie relatywne (dx=1)
        /*data.defaultCode += 'p:\n';
        data.defaultCode += 'p:m=2\n';
        data.defaultCode += 'p:m=2\n';
        data.defaultCode += 'p:\n';
        data.defaultCode += 'j:0,1,dx=1\n';
        data.defaultCode += 'j:1,2,dx=1\n';
        data.defaultCode += 'j:2,3,dx=1\n';*/

        // Trzy pałeczki tworzące gwiazdę (120°), brak opcji “delta”, wrażenie obrócenia względem Part 1 (numer referencyjny 1) stworzone przez zastosowanie absolutnego pozycjonowania
        /*data.defaultCode += 'p:\n';
        data.defaultCode += 'p:1, m=3\n';
        data.defaultCode += 'p:1.50017, -0.865927\n';
        data.defaultCode += 'p:1.50017, 0.865927\n';
        data.defaultCode += 'j:0, 1\n';
        data.defaultCode += 'j:1, 2\n';
        data.defaultCode += 'j:1, 3\n';*/

        // Trzy pałeczki tworzące gwiazdę (120° = 1.047rad), użyto opcję “delta” – Part 2 i 3 przesunięto i obrócono względem Part 1
        /*data.defaultCode += 'p:\n';
        data.defaultCode += 'p:m=3\n';
        data.defaultCode += 'p:\n';
        data.defaultCode += 'p:\n';
        data.defaultCode += 'j:0, 1, dx=1\n';
        data.defaultCode += 'j:1, 2, rz=-1.047, dx=1\n';
        data.defaultCode += 'j:1, 3, rz=1.047, dx=1\n';*/

        // Skęcony łańcuch składający się z 3 pałaczek.
        /*data.defaultCode += 'p:\n';
        data.defaultCode += 'p:\n';
        data.defaultCode += 'p:\n';
        data.defaultCode += 'p:\n';
        data.defaultCode += 'j:0,1,dx=1\n';
        data.defaultCode += 'j:1,2,dx=1,rz=0.5\n';
        data.defaultCode += 'j:2,3,dx=1,rz=0.5\n';*/

        /*data.defaultCode += 'p:1,0,0,rz=0.785\n';
         data.defaultCode += 'p:i="relative position"\n';
         //data.defaultCode += 'p:\n';
         //data.defaultCode += 'p:\n';
         data.defaultCode += 'j:0, 1,dx=1,rz=1.047\n';
         //data.defaultCode += 'j:1, 2, rz=-1.047, dx=1\n';
         //data.defaultCode += 'j:1, 3, rz=1.047, dx=1\n';*/

        return CodeEditor;
    }
);