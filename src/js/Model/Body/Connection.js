/**
 *  A module representing Connection.
 *  @namespace Model/Body
 */
define(
    'Model/Body/Connection',
    ['jquery', 'Model/Body/Item', 'xmlModel/xmlModelService'],
    function ConnectionFactory($, Item, xmlModel) {
        "use strict";

        return function Connection(settings) {
            Item.call(this, settings);

            this.type = 'c';

            this.line = settings.line;
            this.refNumber = settings.refNumber;
            this.model3d = null;
            this.model = {};

            // DEFAULT OPTIONS
            this.defaults = $.extend(true, {}, xmlModel.classProps.c.defaults);
            this.propSequence = xmlModel.classProps.c.propSequence.slice();

            /////////////////// Initialization
            this.parse(settings.code);
        };

    }
);