/**
 *  A module representing Neuron.
 *  @namespace Model/Body
 */
define(
    'Model/Body/Neuron',
    ['jquery', 'Model/Body/Item', 'xmlModel/xmlModelService'],
    function NeuronFactory($, Item, xmlModel) {
        "use strict";

        return function Neuron(settings) {
            Item.call(this, settings);

            this.type = 'n';

            this.line = settings.line;
            this.refNumber = settings.refNumber;
            this.model3d = null;
            this.model = {};

            // DEFAULT OPTIONS
            this.defaults = $.extend(true, {}, xmlModel.classProps.n.defaults);
            this.propSequence = xmlModel.classProps.n.propSequence.slice();

            /////////////////// Initialization
            this.parse(settings.code);
        };

    }
);