/**
 *  A module representing ModelProperties.
 *  @namespace Model/Body
 */
define(
    'Model/Body/ModelProperties',
    ['jquery', 'Model/Body/Item', 'xmlModel/xmlModelService'],
    function ModelPropertiesFactory($, Item, xmlModel) {
        "use strict";

        return function ModelProperties(settings) {
            Item.call(this, settings);

            this.type = 'm';

            this.line = settings.line;
            this.refNumber = settings.refNumber;
            this.model3d = null;
            this.model = {};

            // DEFAULT OPTIONS
            this.defaults = $.extend(true, {}, xmlModel.classProps.m.defaults);
            this.propSequence = xmlModel.classProps.m.propSequence.slice();

            /////////////////// Initialization
            this.parse(settings.code);
        };

    }
);