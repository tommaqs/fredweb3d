/**
 *  A module representing xmlModel.
 *  It takes care of converting XML into JavaScript objects and creates objects with representation of a class.
 *  Everything is based on given XML.
 *
 *  @namespace xmlModel
 */
define(
    'xmlModel/xmlModelService',
    ['jquery', 'Q'],
    function xmlModelServiceModule($, Q) {
        "use strict";

        /////////////////// API
        /**
         * @class
         * @memberof xmlModel
         */
        var xmlModelService = {
            // getters
            get classProps  () { return data.classProps;    },
            get models      () { return data.models;        },
            get xml         () { return data.xml;           },
            // setters
            set classProps  (classProps)    { data.classProps = classProps; },
            set models      (models)        { data.models = models;         },
            set xml         (xml)           { data.xml = xml;               },
            
            // public methods
            initializeModelsFromXML: initializeModelsFromXML
        };

        /////////////////// Data store
        var data = {
            classProps: {},
            interpretedClasses: ['p','j','m','n','c'],
            models: null,
            xml: null
        };

        /////////////////// Private Methods
        /**
         * Create class from "class object" from XML -> JSObject conversion
         *
         * @param {Object} classObj  - Description of class
         * @private
         */
        function _createClass(classObj) {
            var index = data.interpretedClasses.indexOf(classObj['@attrs'].ID);

            if (index > -1) {
                _managePropertiesByType(classObj.PROP, classObj['@attrs'].ID);
            }
        }

        /**
         * Create classes from "class objects" from XML -> JSObject conversion
         *
         * @param {Array} classesArr
         * @private
         */
        function _createClasses(classesArr) {
            for (var i = 0, len = classesArr.length; i < len; i++) {
                _createClass(classesArr[i]);
            }
        }

        /**
         * Method is looking for classes inside of objects after XML -> JSObject conversion
         *
         * @param {Object} rawModels
         * @private
         */
        function _findClasses(rawModels) {
            if ($.isPlainObject(rawModels)) {
                for (var prop in rawModels) {
                    if (rawModels.hasOwnProperty(prop) && prop === 'CLASS') {
                        _createClasses(rawModels[prop]);
                    } else {
                        _findClasses(rawModels[prop]);
                    }
                }
            } else if ($.isArray(rawModels)) {
                for (var i = 0, len = rawModels.length; i < len; i++) {
                    _findClasses(rawModels[i]);
                }
            }
        }

        /**
         * Sets up properties for given class type.
         *
         * @param {Array} properties
         * @param {String} type
         * @private
         */
        function _managePropertiesByType(properties, type) {
            var propertyName = '',
                attrs, lowerCaseAttr;

            xmlModelService.classProps[type] = {
                defaults: {},
                propSequence: []
            };

            for (var i = 0, len = properties.length; i < len; i++) {
                attrs = properties[i]['@attrs'];
                propertyName = properties[i]['@attrs'].ID;

                xmlModelService.classProps[type].defaults[propertyName] = {
                    ommit: true,
                    shortNotation: true
                };
                xmlModelService.classProps[type].propSequence.push(propertyName);

                for (var attr in attrs) {
                    if (attrs.hasOwnProperty(attr)) {
                        lowerCaseAttr = attr.toLowerCase();

                        if (isNaN(parseFloat(attrs[attr]))) {
                            xmlModelService.classProps[type].defaults[propertyName][lowerCaseAttr] = attrs[attr];
                        } else {
                            xmlModelService.classProps[type].defaults[propertyName][lowerCaseAttr] = parseFloat(attrs[attr]);
                        }
                    }
                }

                _setPropInitialValue(xmlModelService.classProps[type].defaults[propertyName], attrs);
            }
        }

        /**
         * Set initial value for property, which depends on default value and type of property value.
         *
         * @param {Object} prop
         * @param {Object} attrs
         * @private
         */
        function _setPropInitialValue (prop, attrs) {
            var typeAttr = attrs['TYPE'],
                type;

            if (typeof attrs['DEF'] === 'undefined') {
                prop.value = 0;
                prop.def = 0;
            } else if (typeof typeAttr === 'string') {
                type = typeAttr.toLowerCase();

                if (type === 'f') {
                    prop.value = parseFloat(attrs['DEF']);
                } else if (prop.type === 'd') {
                    prop.value = parseInt(attrs['DEF']);
                } else {
                    prop.value = attrs['DEF'];
                }
            }
        }

        /**
         * Method converts given xml to plain JavaScript object.
         *
         * @memberof xmlModelService
         * @param xml {Object} - XML Document to convert to plain JavaScript object.
         * @returns {Object} - JS Object from XML document.
         * @private
         */
        function _xmlToJSObject(xml) {
            var obj = {}, attribute, item, nodeName, old;

            if (xml.nodeType === 1) { // 1 = element
                if (xml.attributes.length > 0) {
                    obj["@attrs"] = {};
                    for (var j = 0; j < xml.attributes.length; j++) {
                        attribute = xml.attributes.item(j);
                        obj["@attrs"][attribute.nodeName] = attribute.value;
                    }
                }
            }

            if (xml.hasChildNodes()) {
                for (var i = 0; i < xml.childNodes.length; i++) {
                    item = xml.childNodes.item(i);
                    nodeName = item.nodeName;

                    if (nodeName !== '#text') {
                        if (typeof(obj[nodeName]) === "undefined") {
                            obj[nodeName] = _xmlToJSObject(item);
                        } else {
                            if (typeof(obj[nodeName].push) === "undefined") {
                                old = obj[nodeName];

                                obj[nodeName] = [];
                                obj[nodeName].push(old);
                            }
                            obj[nodeName].push(_xmlToJSObject(item));
                        }
                    }
                }
            }
            return obj;
        }

        /////////////////// Public Methods
        /**
         * Method prepare proper models combined from XML file
         *
         * @memberof xmlModelService
         * @param {String} xmlString
         * @returns {promise}
         */
        function initializeModelsFromXML(xmlString) {
            var deferred = Q.defer();

            // store xml
            xmlModelService.xml = $.parseXML(xmlString);

            // set models from XML
            xmlModelService.models = _xmlToJSObject(xmlModelService.xml);

            // set classes of models
            _findClasses(xmlModelService.models);

            deferred.resolve('XML file parsing is done.');

            return deferred.promise;
        }

        return xmlModelService;
    }
);