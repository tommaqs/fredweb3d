'use strict';

var gulp            = require('gulp'),
    gulpSequence    = require('gulp-sequence'),
    browserSync     = require('browser-sync').create(),
    concat          = require('gulp-concat'),
    del             = require('del'),
    jshint 			= require('gulp-jshint'),
    newer           = require('gulp-newer'),
    mainBowerFiles 	= require('main-bower-files'),
    minifyCss 		= require('gulp-minify-css'),
    minifyHTML 		= require('gulp-minify-html'),
    replace         = require('gulp-replace'),
    sass 		    = require('gulp-sass'),
    sourcemaps 	    = require('gulp-sourcemaps');

var paths = {
    htmls: './src/**/*.html',
    libs: [
        './src/libs/ace-builds/src-min/**/*.*',
        './src/libs/font-awesome-4.2.0/**/*.*',
        './src/libs/jquery-ui-1.11.4.custom/**/*.*',
        './src/libs/threejs-extensions/**/*.*'
    ],
    model: './src/model/**/*.*',
    scripts: './src/js/**/*.js',
    scss: './src/scss/*.scss'
};

gulp.task('browser-sync', function() {
    browserSync.init({
        browser: "google chrome", // this is very OS-based - could be "google chrome"/"chrome"
        port: 7000,
        server: {
            baseDir: "./build/"
        }
    });

    gulp.watch(paths.htmls, ['html']);
    gulp.watch(paths.scripts, ['js-watch']);
    gulp.watch(paths.scss, ['css:own']);
});

gulp.task('clean', function () {
    return del([
        './build/*'
    ]);
});

gulp.task('css:own', function () {
    return gulp
        .src(paths.scss)
        .pipe(sourcemaps.init())
            .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(concat('styles.css'))
        .pipe(gulp.dest('./build/css/'))
        .pipe(browserSync.reload({stream: true}));
});

gulp.task('css:vendors', function () {
    return gulp
        .src(['./bower_components/normalize.css/normalize.css'])
        .pipe(sourcemaps.init())
        .pipe(minifyCss({compatibility: 'ie10'}))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./build/css/'))
        .pipe(browserSync.reload({stream: true}));
});

gulp.task('jshint', function () {
    return gulp
        .src(['gulpfile.js', paths.scripts, '!./src/libs/**/*.js', '!./src/components/**/*.js'])
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

gulp.task('html', function () {
    return gulp
        .src([paths.htmls, '!./src/libs/**/*.html'])
        //.pipe(minifyHTML({conditionals: true, quotes: true}))
        .pipe(gulp.dest('./build/'))
        .pipe(browserSync.reload({stream: true}));
});

gulp.task('libs', function () {
    return gulp
        .src(paths.libs, {base: './src/libs/'})
        .pipe(gulp.dest('./build/libs/'));
});

gulp.task('model', function () {
    return gulp
        .src(paths.model)
        .pipe(gulp.dest('./build/model/'));
});

gulp.task('scripts:own', function() {
    return gulp.src(paths.scripts)
        .pipe(newer('./build/js/'))
        .pipe(gulp.dest('./build/js/'))
        .pipe(browserSync.reload({stream: true}));
});
gulp.task('js-watch', ['scripts:own']);

gulp.task('scripts:vendors', function () {
    return gulp.src(mainBowerFiles({env: 'development'}))
        .pipe(gulp.dest('./build/vendors/'))
        .pipe(browserSync.reload({stream: true}));
});

gulp.task('css', ['css:own', 'css:vendors']);
gulp.task('scripts', ['scripts:own', 'scripts:vendors']);
gulp.task('build', gulpSequence('clean', ['libs', 'model', 'css', 'html', 'scripts']));
gulp.task('serve', gulpSequence('build', 'browser-sync'));

gulp.task('default', ['browser-sync'], function () {});